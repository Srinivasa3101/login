class SessionsController < ApplicationController
    def new
        # No need for anything in here, we are just going to render our
        # new.html.erb AKA the login page
      end
    
      def create
        # Look up employer in db by the email address submitted to the login form and
        # convert to lowercase to match email in db in case they had caps lock on:
        employer = Employer.find_by(email: params[:login][:email].downcase)
        
        # Verify employer exists in db and run has_secure_password's .authenticate() 
        # method to see if the password submitted on the login form was correct: 
        if employer && employer.authenticate(params[:login][:password]) 
          # Save the employer_id in that employer's session cookie:
          session[:employer_id] = employer.id.to_s
          redirect_to employer_index_path, notice: 'Successfully logged in!'
        else
          # if email or password incorrect, re-render login page:
          flash.now.alert = "Incorrect email or password, try again."
          render :new
        end
      end
    
      def destroy
        # delete the saved employer_id key/value from the cookie:
        session.delete(:employer_id)
        redirect_to login_path, notice: "Logged out!"
      end
end

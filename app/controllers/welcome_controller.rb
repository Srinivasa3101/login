class WelcomeController < ApplicationController
  def index
  end

  def login
    user = Employer.find_by(email: params[:email])
   
    if user && user.password == params[:password]
      redirect_to employer_index_path, notice: 'Successfully logged in!'
    else
      flash[:alert] = "User not found."
    end
  end
end

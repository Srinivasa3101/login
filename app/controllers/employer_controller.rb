class EmployerController < ApplicationController
    before_action :authorize, only: [:index]

    def index
        @employer = Employer.all
    end

    def show
        @employer = Employer.find(params[:id])
    end

    def new
        @employer = Employer.new
    end

    def edit
        @employer = Employer.find(params[:id])
      end

    def create
        @employer = Employer.new(logi)
 
        if @employer.save
            redirect_to @employer
        else
            #@employer.errors.each do |error|
                #flash[:errors] = error
            #end
            flash[:errors] = @employer.errors.full_messages
            redirect_to new_employer_path
        end
    end

    def update
        @employer = Employer.find(params[:id])
       
        if @employer.update(logi)
          redirect_to @employer
        else
          render 'edit'
        end
      end

    def destroy
        @employer = Employer.find(params[:id])
        @employer.destroy
        
        session.delete(:employer_id)
        redirect_to welcome_index_path
      end

    private
    def logi
        params.require(:employer).permit(:name, :email, :date_of_birth, :gender, :location, :phone_number, :avatar, :password, :password_confirmation)
    end
end

class Employer < ApplicationRecord
    has_secure_password



    validates :name, length: { minimum: 2, 
    message: "should be atleast two characters" }

    validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

    validates :gender, format: { with: /\A[a-zA-Z]+\z/,
    message: "only allows letters" }

    has_one_attached :avatar
end

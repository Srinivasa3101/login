Rails.application.routes.draw do
  get 'pages/secret'
  get 'welcome/index'

  get '/login'     => 'sessions#new'
	
	# create (post) action for when log in form is submitted:
	post '/login'    => 'sessions#create'
	
	# delete action to log out:
	get '/logout' => 'sessions#destroy'  

  resources :employer
  
  root 'welcome#index'
end

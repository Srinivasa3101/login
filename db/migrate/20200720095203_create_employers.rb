class CreateEmployers < ActiveRecord::Migration[6.0]
  def change
    create_table :employers do |t|
      t.string :name
      t.string :email
      t.string :date_of_birth
      t.text :gender
      t.string :location
      t.integer :phone_number

      t.timestamps
    end
  end
end
